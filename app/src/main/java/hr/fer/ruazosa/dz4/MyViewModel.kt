package hr.fer.ruazosa.dz4

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.*

class MyViewModel: ViewModel() {

    val resultOfDataFetch = MutableLiveData<String>()
    var job: Job? = null

    fun start(time: String) {
        if (job == null) {
            viewModelScope.launch {
                withContext(Dispatchers.IO) {

                    var i = time.toInt()
                    while (isActive) {
                        val fetchedResult = MyRepository.fetchData(i)
                        withContext(Dispatchers.Main) {
                            resultOfDataFetch.value = fetchedResult
                        }
                        i += 1
                    }

                }
            }
        }
    }

    fun stop() {
        viewModelScope.coroutineContext.cancelChildren()
        job = null
    }

}
