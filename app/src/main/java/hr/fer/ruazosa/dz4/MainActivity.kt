package hr.fer.ruazosa.dz4

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    var started = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val myViewModel =
            ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory(application)).get(MyViewModel::class.java)

        myViewModel.resultOfDataFetch.observe(this, Observer {
            timeText.text = it
        })

        startPauseButton.setOnClickListener {
            if (started) {
                startPauseButton.text = "Start"
                started = false
                myViewModel.stop()
            }
            else {
                startPauseButton.text = "Pause"
                started = true
                myViewModel.start(timeText.text.toString())
            }
        }

        resetButton.setOnClickListener {
            timeText.text = "0"
            startPauseButton.text = "Start"
            started = false
            myViewModel.stop()
        }
    }
}